import React, { Component } from "react";
import { ApolloProvider } from "react-apollo";
import ApolloClient from "apollo-boost";
import { InMemoryCache } from "apollo-cache-inmemory";
import Header from './components/header';
import Pages from './pages';
import './App.css';

const cache = new InMemoryCache()
const client = new ApolloClient({
  uri: "http://100.1.231.135:4000/api",
  cache
})

class App extends Component {
  render() {
    return (
      <div className="App">
        <ApolloProvider client={client}>
          <Header />
          <Pages />
        </ApolloProvider>
      </div>
    );
  }
}

export default App;
