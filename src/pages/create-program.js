import React from "react";
import gql from "graphql-tag";
import { redirectTo } from "@reach/router";
import { Mutation } from "react-apollo";
import ProgramForm from "../components/program-form";
import { ALL_PROGRAMS } from "./programs";

const CREATE_PROGRAM = gql`
  mutation CreateProgram(
    $operation: String!,
    $processId: String!,
    $processStep: Int!,
    $productFamily: String!)
  {
    createProgram(operation:
      $operation, processId:
      $processId, processStep:
      $processStep, productFamily:
      $productFamily)
    {
      id
      operation
      processId
      processStep
      productFamily
    }
  }
`

export default function CreateProgram() {
  return (
    <Mutation
      mutation={CREATE_PROGRAM}
      refetchQueries={[{ query: ALL_PROGRAMS }]}
      onCompleted={() => {
        redirectTo("/")
      }}
    >
      {(createProgram, { data }) => {
        return <ProgramForm action={createProgram} />
      }}
    </Mutation>
  )
}
