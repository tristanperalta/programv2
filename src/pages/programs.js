import React from 'react';
import gql from "graphql-tag";
import { Query } from "react-apollo";
import { Link } from "@reach/router";
import DeleteProgram from "./delete-program";

export const ALL_PROGRAMS = gql`
  query AllProgram {
    programs {
      id
      operation
      processId
      processStep
      productFamily
    }
  }
`;

const Programs = () => (
  <Query query={ALL_PROGRAMS}>
    {({ loading, error, data }) => {
      if (loading) return "Loading...";
      if (error) return `Error! ${error.message}`;

      return (
        <ul>
          {data.programs.map(p => {
            return (
              <li key={p.id}>
                <p>id: {p.id}</p>
                <p>operation: {p.operation}</p>
                <p>process ID: {p.processId}</p>
                <p>process step: {p.processStep}</p>
                <p>product family: {p.productFamily}</p>
                <p><DeleteProgram id={p.id} /></p>
                <p><Link to={`program/${p.id}`}>Edit</Link></p>
              </li>
            )
          })}
        </ul>
      );
    }}
  </Query>
);

export default Programs;
