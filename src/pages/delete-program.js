import React from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { ALL_PROGRAMS } from "./programs";

const DELETE_PROGRAM = gql`
mutation deleteProgram($id: Int!) {
  deleteProgram(id: $id) {
    id
    operation
    processId
    processStep
    productFamily
  }
}
`;

const DeleteProgram = ({id}) => {
  let programId = parseInt(id)
  return (
    <Mutation
      mutation={DELETE_PROGRAM}
      refetchQueries={[{ query: ALL_PROGRAMS }]}
    >
      { (deleteProgram, { data }) => {
        return (
          <button onClick={(e) =>
            deleteProgram({variables: { "id": programId }})
          }>Delete</button>
        )
      } }
    </Mutation>
  )
}

export default DeleteProgram;
