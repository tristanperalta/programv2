import React from "react";
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { redirectTo } from "@reach/router";
import ProgramForm from "../components/program-form";
import { ALL_PROGRAMS } from "./programs";

const UPDATE_PROGRAM = gql`
mutation UpdateProgram($id: Int!, $program: UpdateProgramParams!) {
  updateProgram(id: $id, program: $program) {
    id
    operation
    processId
    processStep
    productFamily
  }
}`;

export default function UpdateProgram (props) {
  let program = {
    "id": parseInt(props.programId),
    "operation": "",
    "processId": "",
    "processStep": "",
    "productFamily": ""
  }

  return (
    <Mutation
      mutation={UPDATE_PROGRAM}
      onCompleted={(e) => redirectTo("/")}
      refetchQueries={[{ query: ALL_PROGRAMS }]}
    >
      {(updateProgram, { data, client }) => {
        return <ProgramForm {...program} action={updateProgram} />
      }}
    </Mutation>
  )
}
