import React from 'react';
import { Router } from '@reach/router';
import Programs from './programs';
import CreateProgram from './create-program';
import UpdateProgram from './update-program';

export default function Pages () {
  return (
    <div>
      <Router>
        <Programs path="/" />
        <CreateProgram path="/new" />
        <UpdateProgram path="/program/:programId" />
      </Router>
    </div>
  )
}
