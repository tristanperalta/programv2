import React, { Component } from "react";

export default class ProgramForm extends Component {
  constructor(props) {
    super(props)
    this.state = Object.assign({}, props)
  }

  onChange = (e) => {
    let name = e.target.name
    let value = e.target.value

    if(name === "processStep") {
      value = parseInt(value)
    }

    this.setState({[name]: value})
  }

  onSubmit = (e) => {
    e.preventDefault()
    if (this.props.id) {
      this.onEdit()
    } else {
      this.onCreate()
    }
  }

  onCreate = () => {
    this.props.action({ variables: this.state })
  }

  onEdit = () => {
    this.props.action({ variables: { "id": this.props.id, "program": {
      operation: this.state.operation,
      processId: this.state.processId,
      processStep: this.state.processStep,
      productFamily: this.state.productFamily
    } } })
  }

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <div className="form-group">
          <label>Operation:
            <input type="text" name="operation" onChange={this.onChange} value={this.state.operation} />
          </label>
        </div>
        <div className="form-group">
          <label>Process ID:
            <input type="text" name="processId" onChange={this.onChange} value={this.state.processId} />
          </label>
        </div>
        <div className="form-group">
          <label>Process Step:
            <input type="text" name="processStep" onChange={this.onChange} value={this.state.processStep} />
          </label>
        </div>
        <div className="form-group">
          <label>Product Family:
            <input type="text" name="productFamily" onChange={this.onChange} value={this.state.productFamily} />
          </label>
        </div>
        <button type="submit">Submit</button>
      </form>
    )
  }
}
