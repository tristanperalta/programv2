import React from 'react';
import { Link } from "@reach/router";

export default function Header() {
  return (
    <div>
      <nav>
        <Link to="/">Programs</Link>
        <Link to="/new">Create Program</Link>
      </nav>
    </div>
  )
}
